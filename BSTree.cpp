#include "BSTree.h"

template<typename T>
BSTree<T>:: BSTree():_root(NULL)
{
	
}

template<typename T>
BSTree<T>:: BSTree(T rootVal):_root(rootVal)
{
	
}

template<typename T>
BSTree<T>:: BSTree(const BSTree<T>& other)
{
	this->_root = other._root;
}

template<typename T>
void BSTree<T>:: insertValue(T val)
{
	_root->insertNode(val);
}

template<typename T>
BSNode<T>* BSTree<T>:: search(T val)
{
	return _root->search(val);
}