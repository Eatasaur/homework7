#include <iostream>

template<typename T>
class BSNode
{
public:
	BSNode<T>(T);
	BSNode<T>(const BSNode<T>&);
	bool isLeaf();
	void insertNode(BSNode<T>*);
	BSNode<T>* search(T);
	BSNode<T>& operator=(const BSNode<T>&);
	~BSNode<T>();

private:
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;
};

