#include "BSNode.h"

template<typename T>
BSNode<T>:: BSNode(T data):_data(data)
{

}

template<typename T>
BSNode<T>:: BSNode(const BSNode<T>& other)
{
	this->_data = other._data;
	this->_left = other._left;
	this->_right = other._right;
}

template<typename T>
bool BSNode<T>:: isLeaf()
{
	if((_right == NULL)&&(_left == NULL))
		return true;
	return false;
}

template<typename T>
void BSNode<T>:: insertNode(BSNode<T>* newNode)
{
	if(newNode->_data => _data)
		if(_right == NULL)
			_right = newNode;
		else
			_right->insertNode(newNode);
	if(newNode->_data < _data)
		if(_left == NULL)
			_left = newNode;
		else
			_left->insertNode(newNode);
}

template<typename T>
BSNode<T>* BSNode:: search(T val)
{
	if(val == _data)
		return this;
	if(val > _data)
		_right->search(val);
	else
		_left->search(val);
}

template<typename T>
BSNode<T>& BSNode<T>:: operator=(const BSNode<T>&)
{
	if((this->_data == node._data))&&(this->_left == node._left)&&(this->_right == node._right))
		return *this;
	_data = node._data;
	_left = node._left;
	_right = node._right;
	return *this;
}

template<typename T>
BSNode<T>:: ~BSNode()
{
	
	if(_left != NULL)
	{
		delete _left;
	}
	if(_right != NULL)
	{
		delete _right;
	}
}