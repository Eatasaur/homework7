#include "BSNode.h"

template<typename T>
class BSTree
{
public:
	BSTree();
	BSTree(T);
	void insertValue(T);
	BSNode<T>* search(T);
	BSTree<T>& operator=(const BSTree<T>&);
	~BSTree();

private:
	BSNode<T>* _root;

};